/*
    obj用来接收接口所需要的参数
    接口地址  url
    参数 data
    处理接口成功或者失败的一个回调函数 success  fail
*/ 

var person = {
    name: 'young',
    age: 18   
}

// name=young&age=18
var baseURL = 'https://www.young1024.com:3002'
function ajax(obj){
    var xhr = new XMLHttpRequest();
    // 参数的处理 把对象转换成一个url格式的字符串 
    var params = '';
    for(var p in obj.data){
        params += p+'='+obj.data[p]+'&'
    }
    params = params.slice(0,-1)

    // 请求后的处理
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 && xhr.status == 200){
            var data = JSON.parse(xhr.responseText).data
            obj.success(data)
        }else{
            if(obj.fail){
                obj.fail('网络出错')
            } 
        }
    }
    


    xhr.open('POST',baseURL+obj.url);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params)
}